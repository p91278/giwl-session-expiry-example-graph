import { ResponsiveBar } from '@nivo/bar'
import { useState } from 'react'
import './App.css'

// data is as of 01-14-2021

const pastWeek = [
	{
		'title': 'Total',
		'value': 156
	},
	{
		'title': 'Successful Flows',
		'value': 150
	},
	{
		'title': 'Session Expiration Occurred',
		'value': 20
	},
	{
		'title': 'Session Expired and Ended Flow',
		'value': 6
	}
]

const pastMonth = [
	{
		'title': 'Total',
		'value': 593
	},
	{
		'title': 'Successful Flows',
		'value': 567
	},
	{
		'title': 'Session Expiration Occurred',
		'value': 59
	},
	{
		'title': 'Session Expired and Ended Flow',
		'value': 24
	}
]

const pastYear = [
	{
        'title': 'Total',
		'value': 2105
	},
	{
        'title': 'Successful Flows',
		'value': 2005
	},
	{
        'title': 'Session Expiration Occurred',
		'value': 205
	},
	{
        'title': 'Session Expired and Ended Flow',
		'value': 73
	}
]

const GIWLSessionsGraph = ({ data }) => (
	<ResponsiveBar
		data={data}
		keys={[ 'value' ]}
		indexBy="title"
		layout='horizontal'
		margin={{ top: 50, right: 130, bottom: 100, left: 200 }}
		padding={0.3}
		valueScale={{ type: 'linear' }}
		indexScale={{ type: 'band', round: true }}
		colors="#dbebff"
		// defs={[
		// 	{
		// 		id: 'dots',
		// 		type: 'patternDots',
		// 		background: 'inherit',
		// 		color: '#38bcb2',
		// 		size: 4,
		// 		padding: 1,
		// 		stagger: true
		// 	},
		// 	{
		// 		id: 'lines',
		// 		type: 'patternLines',
		// 		background: 'inherit',
		// 		color: '#eed312',
		// 		rotation: -45,
		// 		lineWidth: 6,
		// 		spacing: 10
		// 	}
		// ]}
		// fill={[
		// 	{
		// 		match: {
		// 			id: 'fries'
		// 		},
		// 		id: 'dots'
		// 	},
		// 	{
		// 		match: {
		// 			id: 'sandwich'
		// 		},
		// 		id: 'lines'
		// 	}
		// ]}
		borderColor="#5499f2"
		borderWidth={3}
		axisTop={null}
		axisRight={null}
		axisBottom={{
			tickSize: 10,
			tickPadding: 10,
			tickRotation: 0,
			// legend: 'GIWL User Flows',
			// legendPosition: 'middle',
			legendOffset: 45
		}}
		axisLeft={{
			tickSize: 5,
			tickPadding: 15,
			tickRotation: -30,
			// legend: 'food',
			// legendPosition: 'middle',
			legendOffset: -40
        }}
        maxValue={2000}
		labelSkipWidth={12}
		labelSkipHeight={12}
		labelTextColor='#000'
		
		// legends={[
		// 	{
		// 		dataFrom: 'keys',
		// 		anchor: 'bottom-right',
		// 		direction: 'column',
		// 		justify: false,
		// 		translateX: 120,
		// 		translateY: 0,
		// 		itemsSpacing: 2,
		// 		itemWidth: 100,
		// 		itemHeight: 20,
		// 		itemDirection: 'left-to-right',
		// 		itemOpacity: 0.85,
		// 		symbolSize: 20,
		// 		effects: [
		// 			{
		// 				on: 'hover',
		// 				style: {
		// 					itemOpacity: 1
		// 				}
		// 			}
		// 		]
		// 	}
		// ]}
		animate={true}
		motionStiffness={90}
        motionDamping={15}
        tooltip={({data}) => (
            <strong style={{ color: '#dbebff' }}>
                {data.title}: {data.value}
            </strong>
        )}
        theme={{
            tooltip: {
                container: {
                    background: '#333',
                    colors: '#fff'
                }
            }
        }}
	/>
)

function App() {

    const [activeData, setActiveData] = useState(pastWeek);
    const [selectedButton, setSelectedButton] = useState('pastWeek')

	return ( 
		<div className="App container">
			<div className={'link-container'}>
				<ul>
					<li>
                        <button className={'time-selection-btn' + (selectedButton === 'pastWeek' ? ' selected': '')} onClick={()=>{setActiveData(pastWeek); setSelectedButton('pastWeek')}}>
                            Past Week
                        </button>
                    </li>
					<li>
                        <button className={'time-selection-btn' + (selectedButton === 'pastMonth' ? ' selected': '')} onClick={()=>{setActiveData(pastMonth); setSelectedButton('pastMonth')}}>
                            Past Month
                        </button>
                    </li>
					<li>
                        <button className={'time-selection-btn' + (selectedButton === 'pastYear' ? ' selected': '')} onClick={()=>{setActiveData(pastYear); setSelectedButton('pastYear')}}>
                            Past Year
                        </button>
                    </li>
				</ul>
			</div>
			<div className={'graph-container'}>
                <div id='graph-title'>GIWL Session Info</div>
				<GIWLSessionsGraph data={activeData} />
			</div>
		</div>
	)
}

export default App;
